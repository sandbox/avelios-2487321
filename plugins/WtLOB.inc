<?php

/**
 * @file
 * Plugin file for the Language of Bindings Thesaurus
 */

class WtLOB extends WebTaxonomy {

  /**
   * Implements WebTaxonomy::autocomplete().
   *
   * Uses the LoB JSON service at http://www.ligatus.org.uk/lob/api/views/search_service.json
   */
  public function autocomplete($string = '') {
    //Be kind to the Ligatus service and start searching at three characters
    if (strlen($string) < 3) {
      return NULL;
    }

    // Allow multiple words in the $string
    $string = trim($string); //ignore space if it is not between words
    /*$string = explode (' ', $string);
    foreach ($string as &$term) {
      $term = $term . '*';
    }

    $estring = $string[0];
    $i = 1;
    while ($i < count($string)) {
      $term = $string[$i];
      $estring .= ' AND ' . $term;
      $i = $i + 1;
    } 
    $string = $estring;*/

    // Build the query.
    $url = 'http://www.ligatus.org.uk/lob/api/views/search_service.json';
    
    $query = $string;
   
    $params = array(
      'key' => $query,
    );

    // Get and parse the JSON query result.
    $options = array (
      //'headers' => array('Accept' => 'text/html'), //this is required because the Getty response is temperamental
    );
    $results = drupal_http_request($url . '?' . http_build_query($params), $options);
    
    if (isset($results->error)) {
      drupal_set_message(t($results->error));
      return NULL;
    }
    $results = json_decode($results->data, TRUE);

    // Build the data structure WebTaxonomy::autocomplete() expects.
    $term_info = array();
    foreach ($results as $result) {
      $term_name = (string) $result['preflabel'];
      //some LoB terms do not have scope notes so check first
      $scope_note = isset($result['scopenote']) ? (string) $result['scopenote'] : "";
      $uri = (string) $result['uri'];
      $term_info[$term_name] = array(
        'name' => $term_name,
        'web_tid' => $uri,
        'description' => $scope_note,
      );
    }
    return $term_info;
  }

  /**
   * Implements WebTaxonomy::fetchTerm().
   *
   * Reads label information from LOD representation in JSON format.
   *
   * For resource http://vocab.getty.edu/resource/aat/300256003, the JSON file
   * is http://vocab.getty.edu/resource/aat/300256003.json.
   */
  public function fetchTerm($term) {
    $web_tid = $term->web_tid[LANGUAGE_NONE][0]['value'];
    if (!empty($web_tid)) {
      //$web_tid is something like http://purl.org/lob/300011851 , we need the last number
      $rev_web_tid = array_reverse(explode('/', $web_tid));
      $lob_id = $rev_web_tid[0];
      
      // Build the query.
      $url = 'http://www.ligatus.org.uk/lob/api/views/search_service.json';
      $query = $lob_id;
   
      $params = array(
        'uri' => $query,
      );
      
    }
    else {
      // @todo If there is no Web Tid, check by name.
    }

    // Get and parse the JSON query result.
    $options = array (
      //'headers' => array('Accept' => 'text/html'), //this is required because the Getty response is temperamental
    );

    $results = drupal_http_request($url . '?' . http_build_query($params), $options);
    if (isset($results->error)) {
      drupal_set_message(t($results->error));
      return NULL;
    }
    $results = json_decode($results->data, TRUE);

    $term_info = array();
    foreach ($results as $result) {
      $term_name = $result['preflabel'];
      $scope_note = $result['scopenote'];
      $term_info[$term_name] = array(
        'name' => $term_name,
        'web_tid' => $web_tid,
        'description' => $scope_note,
      ); 
    }
    return $term_info;
  }
}
